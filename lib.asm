section .text
global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
%define STDOUT 1
%define STDIN 0
%define TAB `\t`
%define NEWLINE `\n`
%define SPACE ` `
%define WRITE_SYSCALL 1
%define EXIT_SYSCALL 60




; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, EXIT_SYSCALL
    syscall 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax,rax
    .loop:
        cmp byte[rdi+rax], 0    ; ищем стоп символа
        je .endPoint            ; если нашли, то выходим
        inc rax                 ; если не нашли, то увеличиваем счетчик+=1
        jmp .loop
    .endPoint:
        ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length          ; посчитаем длину строки
    pop rsi; передадим строку
    mov rdx, rax                ; передадим длину строки
    mov rdi, 1                  ; дескриптор stdout
    mov rax, 1                  ; функция write
    syscall
    ret


; Принимает код символа и выводит его в stdout
print_char:
    xor rax, rax
    push rdi                ; сохраняем символ в стек
    mov rdx, 1              ; размер будет 1 байт
    mov rsi, rsp            ; адрес выводимого символа записываем в rsi
    mov rdi, STDOUT         ; дескриптор stdout
    mov rax, 1              ; функция write
    syscall
    pop rdi
    ret



; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEWLINE
    jmp print_char




; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi                ; перенесем число в rax
    xor rdx, rdx                ; занулим rdx
    mov rcx, 10              
    mov r10, rsp                ; для сохранения вершины стека
    dec rsp                      
    mov byte[rsp],dl            ; добавим нуль-терминатор
    .loop: 
        xor rdx, rdx                
        div rcx
        add rdx,'0'             ; первод в ASCII символ
        dec rsp                 
        mov byte[rsp],dl
        cmp rax,0               ; rax>0 продолжим цикл
        ja .loop
    mov rdi,rsp                 ; ставим указатель на строку 
    push r10                    ; заносим в стек старую вершину
    call print_string           ; Напечатаем число, как строку
    pop rsp                     ; возврат стека в исходное положение
    ret







; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi,rdi
	jge .number
	push rdi
	mov rdi, '-'
	call print_char
	pop rdi
	neg rdi
	.number:
		call print_uint
	ret






; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
    .loop:
		mov r8b, byte[rsi+rcx]
		cmp byte[rdi+rcx], r8b
		jne .end
		cmp byte[rdi+rcx], 0
		je .good
		inc rcx
		jmp .loop
    .good:
		mov rax, 1
		ret
	.end:
		xor rax, rax
		ret
	









; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rdi, STDIN              ; дескриптор stdin
    mov rax, 0              ; read в rax
    push 0                  ; резервируем одно место
    mov rdx, 1              ; размер 1 байт
    mov rsi, rsp            ; указатель на место в стеке, куда положим символ
    syscall
    pop rax                 ; считанный символ достаём из стека в rax
    ret









; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:     
    xor rax,rax
    mov rcx, 0          
    .loop:
        cmp rcx, rsi  
        jge .nobuffer  
        mov r12, rsi 
        mov r13, rdi  
        mov r14, rcx   
        call read_char  
        mov rcx, r14         
        mov rdi, r13 
        mov rsi, r12
        cmp rax, TAB   
        je .nosymbol       
        cmp rax, SPACE
        je .nosymbol
        cmp rax, NEWLINE
        je .nosymbol
        mov [rdi+rcx], rax 
        
        test rax, rax      
        je .end         
        inc rcx         
        jmp .loop 
    .nobuffer:         
        xor rax, rax
        xor r12, r12
        xor r13, r13
        xor r14, r14
        ret     
    .nosymbol:
        test rcx, rcx    
        jne .end       
        jmp .loop       
    
    .end:            
        mov rdx, rcx
        mov rax, rdi
        xor r12, r12
        xor r13, r13
        xor r14, r14  
        ret



 





; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    mov rax, 0
	mov rcx, 0 
    mov rsi, 0 
    mov r10, 10
	
    .loop:
    	movzx r9, byte[rdi+rcx]     
        sub r9, '0'                  
        cmp r9, 0   
        jl .end 
        cmp r9, 9
        jg .end 
        mul r10                     
        add rax, r9                 
        inc rcx                    
        jmp .loop 
		
    .end:
		mov rdx, rcx
    	ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось

parse_int:
    cmp byte[rdi], '+'          ; cравним с '+'
    je .positive
    cmp byte[rdi], '-'          ; cравним с '-' 
    je .negative                   
    jne parse_uint              ; eсли не равен, то запустим обработку для беззнакового
    .positive:                      
        inc rdi                 
        call parse_uint         
        inc rdx                 
        ret
    .negative:
        inc rdi                 
        call parse_uint
        inc rdx
        neg rax                 
        ret 




; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0

string_copy: ; rsi - указывает на буффер, rdx - указывает на размер буффера, rdi - указывает на строку
     xor rax, rax               
    .loop:
        mov r9b, byte[rdi] ; копирование из строки в буффер при помощи промежуточного регистра r9b
        mov byte[rsi], r9b ; 
        cmp byte[rsi], 0x0  
        jz .good
        inc rsi
        inc rdi
        inc rax                 
        cmp rax, rdx ; сравним длины строки и буффера
        jge .end
        jmp .loop
    .good:
        ret 
    .end: 
        xor rax,rax             
        ret
