%include "dict.inc"
%include "lib.inc"
%include "words.inc"

%define BUFFER_SIZE 256

section .bss
	buffer: resb BUFFER_SIZE

section .rodata
	too_long_one: db "It is too long string!", 10, 0
	not_found_one: db "The string is not found!", 10, 0
        
section .text
	global _start

_start:
        mov     rdi, buffer        	
        mov     rsi, BUFFER_SIZE           	
        call    read_word          	
        test    rax, rax           	
        jz      .too_long_word     	
        mov     rsi, first_word    
        mov     rdi, buffer        	
        call    find_word         	
        test    rax, rax           	
        jz      .not_found_word    	
        mov     rdi, rax           	
        add     rdi, 8                 
        push    rdi                     
        call    string_length          
        pop     rdi                     
        add     rdi, rax                
        inc     rdi                     
        call    print_string           
        call    print_newline           
        xor     rdi, rdi                
        call    exit                    
        
.too_long_word:
        mov     rdi, too_long_one    	; "The word is too long!" -> rdi
        jmp     .show_the_error     	; goto show_the_error
        
.not_found_word:
        mov     rdi, not_found_one   	; "The word is not found!" -> rdi
        
.show_the_error:
        push    rdi                     ; rdi -> st_top
        call    string_length           ; goto string_length
        pop     rsi                     ; restore rsi
        mov     rdx, rax                ; rax -> rdx (length)
        mov     rax, 1             	; prepare for syscall
        mov     rdi, 2             	; prepare for syscall
        syscall                         ; print err
        call    exit                    ; goto exit
