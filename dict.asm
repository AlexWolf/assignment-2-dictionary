%include "lib.inc"

global find_word

find_word:
    push r12            
    test rsi, rsi      
    jz .exit            
    
.loop:
    mov r12, rsi        
    add rsi, 8  	
    call string_equals  
    test rax, rax      
    jz .take_next       
    mov rax, r12        
    pop r12             
    ret
    
.take_next:
    mov rsi, r12        
    mov rsi, [rsi]      
    test rsi, rsi      
    jnz .loop           
    xor rax, rax        
    pop r12             
    ret

.exit:
    xor rax, rax        
    ret
